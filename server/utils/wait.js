export default function wait(promise) {  
    return promise.then(data => {
       return [null, data]
    })
    .catch(err => [err])
 }