import Router from 'koa-router'
import { PubgAPI, REGION, SEASON, MATCH } from 'pubg-api-redis'
import wait from '../utils/wait'

const router = new Router()

const example = {"region":"as","defaultRegion":"agg","season":"2018-01","defaultSeason":"2018-01","match":"squad","lastUpdated":"2018-01-01T06:05:21.5795875Z","playerName":"leichtjoon","avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/15/15734d24c2296860a105331c2b5d9b0d98d92cd3.jpg","performance":{"killDeathRatio":"0","winRatio":100,"timeSurvived":1811.29,"roundsPlayed":1,"wins":1,"winTop10Ratio":1,"top10s":1,"top10Ratio":100,"losses":"0","winPoints":1079},"skillRating":{"rating":1288,"bestRating":1287.87,"bestRank":"0"},"perGame":{"damagePg":157.82,"headshotKillsPg":"0","healsPg":2,"killsPg":2,"moveDistancePg":8769,"revivesPg":"0","roadKillsPg":"0","teamKillsPg":"0","timeSurvivedPg":1811.29,"top10sPg":1},"combat":{"kills":2,"assists":"0","suicides":"0","teamKills":"0","headshotKills":"0","headshotKillRatio":"0","vehicleDestroys":"0","roadKills":"0","dailyKills":2,"weeklyKills":2,"roundMostKills":2,"maxKillStreaks":1,"weaponAcquired":"0"},"survival":{"days":1,"longestTimeSurvived":1811.29,"mostSurvivalTime":1811.29,"avgSurvivalTime":1811.29},"distance":{"walkDistance":2119.86,"rideDistance":6649.13,"moveDistance":8769,"avgWalkDistance":2119.86,"avgRideDistance":6649.13,"longestKill":299.02},"support":{"heals":2,"revives":"0","boosts":2,"damageDealt":157.82,"dBNOs":2},"rankData":{}}

function test() {
    return new Promise((resolve, reject) => setTimeout(() => resolve(example), 1500))
}

/*
const api = new PubgAPI({
    apikey: 'd17cb953-92f1-4968-a946-c3b57fef68c9',
    redisConfig: {
      host: '127.0.0.1',
      port: 6379,
      expiration: 240,
    },
})
*/
/*
    api.getProfileByNickname(ctx.params.id).then(profile => {
        const data = profile.content
        const stats = profile.getStats({
            region: 'as',
            season: SEASON.EA2017pre6,
            match: MATCH.SQUAD
        })
        ctx.is('application/json')
        ctx.body = stats
    }).catch(err => {
        ctx.body = err
    })
    */
router.get('/stats/:id', async ctx => {
    ctx.body = await test()
    /*
    const [err, profile] = await wait(api.getProfileByNickname(ctx.params.id))
    if(err) return ctx.body = err
    const stats = profile.getStats({
        region: 'as',
        season: SEASON.EA2017pre6,
        match: MATCH.SQUAD
    })
    ctx.is('application/json')
    ctx.body = stats
    */
})

export default router