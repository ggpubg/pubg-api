import Koa from 'koa'
import cors from '@koa/cors'
import Router from './routes'

const app = new Koa()

app.use(cors())
app
  .use(Router.routes())
  .use(Router.allowedMethods())
app.listen(4000)